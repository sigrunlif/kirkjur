var createStory = function(){
    return "<p> this is my story </p>";
}

var renderStory = function(){
    document.querySelector('#main').innerHTML = createStory();
}

module.exports = {
    createStory,
    renderStory
};

// Þetta er það sama
// module.exports = {
//     'createStory': createStory,
//     'renderStory': renderStory
// };