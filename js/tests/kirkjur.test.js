document.body.innerHTML = '<div id="main"></div>'

var { createStory, renderStory } = require('../main.js')

test('The main text should include a <p> element', function (){
    expect( createStory() ).toMatch(/<p>/);
});

test('There should be a <p> element inside the main div', function(){
    var getFromDom = function(){
        renderStory();
        return document.querySelector('#main').innerHTML;
    }
    expect( getFromDom() ).toMatch(/<p>/);
});